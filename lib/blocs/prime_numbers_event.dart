abstract class PrimeNumbersEvent {}

class CalculatePrimeNumbersEvent extends PrimeNumbersEvent {
  final List<int> numbers;

  CalculatePrimeNumbersEvent(this.numbers);
}
