import 'dart:async';
import 'dart:math';
import 'package:bloc/bloc.dart';
import 'package:prime_numbers_test/blocs/prime_numbers_event.dart';
import 'package:prime_numbers_test/blocs/prime_numbers_state.dart';

class PrimeNumbersBloc extends Bloc<PrimeNumbersEvent, PrimeNumbersState> {
  PrimeNumbersBloc() : super(InitialPrimeNumbersState()) {
    on<CalculatePrimeNumbersEvent>((event, emit) async {
      List<String> results = [];
      for (var number in event.numbers) {
        results.add(await _calculateNumber(number));
      }
      emit(CalculatedPrimeNumbersState(results));
    });
  }

  Future<String> _calculateNumber(int number) async {
    if (number == 1) return '$number is not a Prime number';
    if (number == 2) return '$number is a Prime number';
    var limit = sqrt(number).ceil();
    for (int i = 2; i <= limit; ++i) {
      if (number % i == 0) return '$number is not a Prime number';
    }
    return '$number is a Prime number';
  }
}
