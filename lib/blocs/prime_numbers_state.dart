abstract class PrimeNumbersState{}

class InitialPrimeNumbersState extends PrimeNumbersState{
  
}
class CalculatedPrimeNumbersState extends PrimeNumbersState{
  final List<String> results;

  CalculatedPrimeNumbersState(this.results);
}
