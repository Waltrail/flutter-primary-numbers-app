import 'package:flutter/material.dart';

class MainAppButton extends StatelessWidget {
  final Color color;
  final Color borderColor;
  final double? width;
  final String text;
  final void Function()? onTap;

  const MainAppButton(
    this.text, {
    Key? key,
    this.onTap,
    this.color = Colors.black,
    this.borderColor = Colors.black,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(5),
        child: Container(
          decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: borderColor)),
          padding: const EdgeInsets.all(12.0),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: color.computeLuminance() > 0.6
                      ? Colors.black
                      : Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
