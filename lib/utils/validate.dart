String? validatePrimeNumber(String? value) {
  int? number = int.tryParse(value ?? '');
  if (number == null) {
    return "Incorrect input";
  } else if (number < 0) {
    return "Number supposed to be positive";
  }
}
