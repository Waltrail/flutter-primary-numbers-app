import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prime_numbers_test/blocs/bloc.dart';
import 'package:prime_numbers_test/utils/validate.dart';
import 'package:prime_numbers_test/widgets/app_buttons.dart';
import 'package:prime_numbers_test/widgets/app_text_form_field.dart';

class PrimeNumbersScreen extends StatefulWidget {
  const PrimeNumbersScreen({Key? key}) : super(key: key);

  @override
  PrimeNumbersScreenState createState() {
    return PrimeNumbersScreenState();
  }
}

class PrimeNumbersScreenState extends State<PrimeNumbersScreen> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController inputNumber1 = TextEditingController();
  final TextEditingController inputNumber2 = TextEditingController();
  final TextEditingController inputNumber3 = TextEditingController();

  PrimeNumbersScreenState();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _reset() {
    inputNumber1.text = '';
    inputNumber2.text = '';
    inputNumber3.text = '';
  }

  _onSubmit() async {
    if (_formKey.currentState!.validate()) {
      List<int> numbers = [
        int.parse(inputNumber1.text),
        int.parse(inputNumber2.text),
        int.parse(inputNumber3.text),
      ];
      BlocProvider.of<PrimeNumbersBloc>(context).add(CalculatePrimeNumbersEvent(
        numbers,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        body: BlocConsumer<PrimeNumbersBloc, PrimeNumbersState>(listener: (
          BuildContext context,
          PrimeNumbersState currentState,
        ) {
          if (currentState is CalculatedPrimeNumbersState) {
            showModalBottomSheet(
                context: context,
                builder: (context) {
                  return SizedBox(
                    height: 240,
                    child: Column(
                      children: [
                        Container(
                          width: double.infinity,
                          color: Colors.black,
                          child: const Padding(
                            padding: EdgeInsets.all(24.0),
                            child: Text("Calculated prime numbers:",
                                style:
                                    TextStyle(fontSize: 24, color: Colors.white)),
                          ),
                        ),
                        const SizedBox(height: 10),
                        Column(
                          children: currentState.results
                              .map((result) => Padding(
                                    padding: const EdgeInsets.all(12),
                                    child: Text(result,
                                        style: const TextStyle(fontSize: 18)),
                                  ))
                              .toList(),
                        ),
                      ],
                    ),
                  );
                });
          }
        }, builder: (
          BuildContext context,
          PrimeNumbersState currentState,
        ) {
          return Center(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(top: 18.0, left: 8, right: 8),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Please input positive number",
                            style: TextStyle(fontSize: 18),
                          )),
                    ),
                    AppTextFormField(
                      controller: inputNumber1,
                      validator: validatePrimeNumber,
                      keyboardType: TextInputType.number,
                    ),
                    AppTextFormField(
                      controller: inputNumber2,
                      validator: validatePrimeNumber,
                      keyboardType: TextInputType.number,
                    ),
                    AppTextFormField(
                      controller: inputNumber3,
                      validator: validatePrimeNumber,
                      keyboardType: TextInputType.number,
                    ),
                    const SizedBox(height: 30),
                    MainAppButton(
                      'Calculate prime numbers',
                      onTap: () {
                        _onSubmit();
                      },
                    ),
                    const SizedBox(height: 30),
                    MainAppButton(
                      'Reset',
                      color: Colors.red,
                      borderColor: Colors.red,
                      onTap: () {
                        _reset();
                      },
                    ),
                    const SizedBox(height: 50),
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
