import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prime_numbers_test/blocs/prime_numbers_bloc.dart';
import 'package:prime_numbers_test/screens/prime_numbers_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Prime numbers',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: _primeNumbers(context),
    );
  }

  Widget _primeNumbers(BuildContext appContext) {
    return BlocProvider<PrimeNumbersBloc>(
        create: (context) {
          return PrimeNumbersBloc();
        },
        child: const PrimeNumbersScreen());
  }
}